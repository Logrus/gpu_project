#pragma once
#ifndef NLMF_H_
#define NLMF_H_
#include <cstdio>
#include <CMatrix.h>

CMatrix<float> nonLocalMeanCudaNaive(const CMatrix<float> &image, int window_radius, int patch_radius, float sqr_sigma);
CMatrix<float> nonLocalMeanCudaSymmetries(const CMatrix<float> &image, int window_radius, int patch_radius, float sqr_sigma);


#endif // NLMF_H_