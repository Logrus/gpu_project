#ifndef AUXCU_H_
#define AUXCU_H_

#ifndef CLIP
#define CLIP(min, val, max) (fminf(max, fmaxf(min,val)))
#endif

#ifndef DIV_UP
#define DIV_UP(a, b) (std::ceil((a)/(float)(b)))
#endif

#endif /* AUXCU_H */