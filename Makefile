project: nlm.cc Makefile
	nvcc -O3 -Xcompiler="-fopenmp" -arch=compute_20 -code=compute_20 -I. nlm.cc nlmf.cu timer.cc load_sequence.cpp -o NLM_Project
clean:
	rm NLM_Project
